# training

Descrypt's training documents.

Clone the entire repo or paste file contents into a text editor.

Note: Where applicable, do not rely too much (or preferably, at all!) on syntax checking and highlighting in your text editor. Dissable these features or use a simple text editor (such as notepad or nano).

## Currently supported topics
### Programming
- Python

