'''
Expected output:
H
E
L
L
O
'''


count = 0
while count != 5:
    if count == 0:
        print('H')
    if count == '1':
        print('E')
    if count == 2:
        print('L')
    if count == 3:
        print('L')
    else:
        print('O')
    count = count + 1
