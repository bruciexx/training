'''
Expected output:
Guido is a really cool person!!!
'''

def coolmaker(name):
    cool = name + ' is a really cool person!!!'

cool_person = coolmaker('Guido')

print(cool_person)
