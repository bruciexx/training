'''
Expected output:
python
bash
go
'''


languages = [
    'python'
    'bash',
    'go'
]

for language in languages:
    print(language)
