'''
Expected output:
ok
ok
'''

username = 'python'
password = 'password'

if username == 'python':
    if 'password' == 'password':
        print('ok')

username = 'python'
password = 'newpassword'

if username == 'python':
    if 'password' == 'newpassword':
        print('ok')
